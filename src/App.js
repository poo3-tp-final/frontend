import React from 'react';
import './App.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import {Navigate, Route, Routes} from "react-router-dom";
import AuthPage from "./app/pages/AuthPage";
import {Fragment} from "react";
import MainPage from "./app/pages/MainPage";
import {useSelector} from "react-redux";
import ProductPage from "./app/pages/ProductPage";
import ReporteBalanceMensual from "./app/pages/ReporteBalanceMensual";
import ProductEditPage from "./app/pages/ProductEditPage";
import ProductCreatePage from "./app/pages/ProductCreatePage";

function App() {
    const isLoggedIn =useSelector(state => state.auth.isLoggedIn);
    return (
        <Fragment>
                <Routes>
                    {!isLoggedIn &&   <Route path='/' element={<Navigate to={"/auth"}></Navigate>}/>}
                    {isLoggedIn && <Route path='/' element={<Navigate to={"/main"}></Navigate>}/>}
                    {isLoggedIn && <Route path='/auth' element={<Navigate to={"/main"}></Navigate>}/>}
                    {!isLoggedIn && <Route path='/auth' element={<AuthPage></AuthPage>}/>}
                    {isLoggedIn && <Route path='/main' element={<MainPage></MainPage>}/>}
                    {isLoggedIn && <Route path='/products' element={<ProductPage></ProductPage>} >

                        </Route>}
                    {isLoggedIn &&   <Route path='/products/:productId' element={<ProductEditPage></ProductEditPage>} />}
                    {isLoggedIn &&   <Route path='/products/create' element={<ProductCreatePage></ProductCreatePage>} />}
                    {isLoggedIn && <Route path='/balance-mensual' element={<ReporteBalanceMensual></ReporteBalanceMensual>}/>}
                    <Route path="*" element={<Navigate to="/"></Navigate>}/>
                </Routes>
        </Fragment>
    );
}

export default App;
