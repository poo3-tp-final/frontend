import {Fragment} from "react";
import AppBar from "@mui/material/AppBar";
import {Toolbar, Typography} from "@mui/material";

const CustomFooter = (props) => {
    return (
        <Fragment>
            <AppBar position="fixed" color="primary" sx={{top: 'auto', bottom: 0}}>
                <Toolbar>
                    <Typography
                        variant="h5"
                        align="center"
                        noWrap
                        component="div"
                        sx={{flexGrow: 1, display: {xs: 'none', sm: 'block'}}}
                    >
                        {props.title}
                    </Typography>
                    <Typography
                        variant="h6"
                        align="center"
                        noWrap
                        component="div"
                        sx={{flexGrow: 1, display: {xs: 'block', sm: 'none'}}}
                    >
                        {props.shortTitle}
                    </Typography>

                </Toolbar>
            </AppBar>
        </Fragment>
    );
}

export default CustomFooter;