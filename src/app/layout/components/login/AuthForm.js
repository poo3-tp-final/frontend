import {useRef, useState} from 'react';
import constans from '../../../store/constans';
import classes from './AuthForm.module.css';
import {authActions} from '../../../store/slices/auth-slice';
import { useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";

const AuthForm = () => {
    const [isLogin, setIsLogin] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const emailInputRef = useRef();
    const passInputRef = useRef();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const switchAuthModeHandler = () => {
        setIsLogin((prevState) => !prevState);
    };

    const submitFormAuthHandler = (event) => {
        event.preventDefault();
        const enteredEmail = emailInputRef.current.value;
        const enteredPassword = passInputRef.current.value;
        setIsLoading(true);
        //optional valid
        let URL;
        if (isLogin) {
            URL = constans.API_URL + 'login';
            console.log(URL);
        } else {
            URL = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAiu7YJZR5DWJTjSrnpmpUsOPbj1CVnX90';
        }

        fetch(URL,
            {
                method: 'POST',
                body: JSON.stringify({
                    username: enteredEmail,
                    password: enteredPassword
                }),
                headers: {
                    'content-type': 'application/json'
                }
            }).then(res => {
            setIsLoading(false);
            if (res.ok) {
                console.log(res)
                return res.json()
            } else {
                return res.json().then(data => {
                    let errorMessage = 'Authentication Failed!';

                    throw new Error(errorMessage);

                });
            }
        }).then(data=>{
            const expirationTime = new Date(new Date().getTime() + (+300000 * 1000));
            //authCtx.login(data.idToken, expirationTime.toISOString());
            dispatch(authActions.login({token: data.token, expirationTime: expirationTime.toISOString(), user_id: data.user_id, email: data.email }));
            navigate('/', {replace:true})
        }).catch(err=>alert(err.message))

    };

    return (
        <section className={classes.auth}>
            <h1>{isLogin ? 'Login' : 'Registrarse'}</h1>
            <form onSubmit={submitFormAuthHandler}>
                <div className={classes.control}>
                    <label htmlFor='email'>Usuario</label>
                    <input type='text' id='email' required ref={emailInputRef}/>
                </div>
                <div className={classes.control}>
                    <label htmlFor='password'>Contraseña</label>
                    <input type='password' id='password' required ref={passInputRef}/>
                </div>
                <div className={classes.actions}>
                    {!isLoading && <button>{isLogin ? 'Login' : 'Crear cuenta'}</button>}
                    {isLoading && <p>Sending Request...</p>}
                    <button
                        type='button'
                        className={classes.toggle}
                        onClick={switchAuthModeHandler}
                    >
                        {isLogin ? 'Crear nueva cuenta' : 'Ya tengo una cuenta'}
                    </button>
                </div>
            </form>
        </section>
    );
};

export default AuthForm;