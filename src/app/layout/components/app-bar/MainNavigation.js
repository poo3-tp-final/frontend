import {Box, Toolbar, Typography} from "@mui/material";
import AppBar from "@mui/material/AppBar";

const MainNavigation = (props) => {
    return (
        <Box sx={{flexGrow: 1}}>
            <AppBar position="static">
                <Toolbar>

                    <Box sx={{display: {xs: 'none', sm: 'block'}, m: 2}}><img src={props.urlLogo} alt="Kitten"
                                                                              height="45"
                                                                              width="35"
                    />
                    </Box>
                    <Typography
                        variant="h5"
                        noWrap
                        component="div"
                        sx={{flexGrow: 1, display: {xs: 'none', sm: 'block'}}}
                    >
                        {props.title}
                    </Typography>
                    {props.children}
                </Toolbar>
            </AppBar>
        </Box>

    );
}

export default MainNavigation;