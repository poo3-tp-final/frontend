import { createSlice} from "@reduxjs/toolkit";


const calculateRemainingTime = (expirationTime) => {
    const currentTime = new Date().getTime();
    const adjExpirationTime = new Date(expirationTime).getTime();

    const remainingDuration = adjExpirationTime - currentTime;

    return remainingDuration;
};

const retriveStoredToken = () => {
    const storedToken = localStorage.getItem('token');
    const storedExpirationTime = localStorage.getItem('expirationTime');

    const remainingTime = calculateRemainingTime(storedExpirationTime);

    if (remainingTime <= 3600) {
        localStorage.removeItem('token');
        localStorage.removeItem('expirationTime');
        return null;
    }

    return {
        token: storedToken,
        duration: remainingTime
    }}



const authInitialState = ()=>{
    const tokenData = retriveStoredToken();
    let initialState;
    if (tokenData) {
        initialState = {
            token: tokenData.token,
            isLoggedIn: true
        };
    }else{
        initialState = {
            token: '',
            isLoggedIn: false
        };
    }

    return initialState;

}


const authSlice = createSlice({
    name: 'authentication',
    initialState: authInitialState,
    reducers: {
        login(state, action){
            localStorage.setItem('token', action.payload.token);
            localStorage.setItem('expirationTime', action.payload.expirationTime);
            localStorage.setItem('userId', action.payload.user_id);
            localStorage.setItem('email', action.payload.email);
            state.isLoggedIn = true;
        },
        logout(state){
            localStorage.removeItem('token');
            localStorage.removeItem('expirationTime');
            localStorage.removeItem('userId');
            localStorage.removeItem('email');
            state.isLoggedIn = false;
        },

    }
});


export const authActions = authSlice.actions;
export default authSlice.reducer;