import React, {useEffect, useState} from "react";
import Layout from "../layout/Layout";
import Grid from "@mui/material/Grid";
import CustomBreadcrumbs from "./components/CustomBreadcrumbs";
import {useNavigate} from "react-router-dom";
import SaveIcon from '@mui/icons-material/Save';
import {
    Button,
    Card,
    FormControl,
    FormGroup,InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import constants from "../store/constans";

const ProductCreatePage = () => {
    const navigate = useNavigate()
    const pathProductCreate = constants.API_URL + 'product/create';
    const pathCategoriesList = constants.API_URL + 'categories';
    const pathBrandsList = constants.API_URL + 'brands';
    const pathTypesList = constants.API_URL + 'types';
    const [product, setProduct] = useState([]);
    const [categories, setCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [types, setTypes] = useState([]);
    const [isReady, setIsReady] = useState(false);

    const handleChange = (event) => {
        setProduct({
            ...product,
            [event.target.name]: event.target.value,
        })
    };

    const createHandler = async () => {
        const response = await fetch(pathProductCreate, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)

        })
        navigate('/products')

    }


    const getCategoriesData = async () => {
        const response = await fetch(pathCategoriesList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const categories = await response.json()
        setCategories(categories)
    }

    const getTypesData = async () => {
        const response = await fetch(pathTypesList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const types = await response.json()
        setTypes(types)
    }

    const getBrandData = async () => {
        const response = await fetch(pathBrandsList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const brands = await response.json()
        setBrands(brands)
    }


    useEffect(() => {
           getCategoriesData().then(() => {
            getTypesData()
        }).then(() => {
            getBrandData()
        }).then(() => {
            setIsReady(true)
        }).catch(() => {
            alert('Algo salio Mal!')
        })
    }, [])

    return (
        <Layout>
            <Grid item xs={12} md={12} lg={12}>
                <CustomBreadcrumbs title={"Product/Create"}></CustomBreadcrumbs>
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
                <Card sx={{backgroundColor: "#c5cae9", padding: 10}}>
                    <Typography sx={{marginBottom: 3}} component="div" variant="h3">
                       Crear Producto
                    </Typography>
                    {isReady && <FormGroup>
                        <FormControl>
                            <TextField
                                sx={{marginTop: 3, marginBottom: 3}}
                                label="Descripcion"
                                name="description"
                                onChange={handleChange}
                            />
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Categoria</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                name="category_id"
                                onChange={handleChange}
                            >
                                {categories.map(item => <MenuItem key={item.id}
                                                                  value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                labelId="demo-simple-select-label"
                                name="type_id"
                                id="demo-simple-select"
                                onChange={handleChange}
                            >
                                {types.map(item => <MenuItem key={item.id}
                                                             value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Marca</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                labelId="demo-simple-select-label"
                                name="brand_id"
                                id="demo-simple-select"
                                onChange={handleChange}
                            >
                                {brands.map(item => <MenuItem key={item.id}
                                                              value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="Código proveedor"
                                sx={{marginTop: 3, marginBottom: 3}}
                                name="supplier_code"
                                onChange={handleChange}
                            />
                        </FormControl>

                        <Divider/>
                        <div>
                            <Grid item xs={4} md={6} lg={6}>
                                <Button sx={{marginTop: 3, justifyContent: "center"}} variant="contained"
                                        endIcon={<SaveIcon/>} onClick={createHandler}>
                                    Save
                                </Button>
                            </Grid>
                        </div>

                    </FormGroup>}
                </Card>
            </Grid>

        </Layout>);
}

export default ProductCreatePage;