import React, {useEffect, useState} from "react";
import Layout from "../layout/Layout";
import Grid from "@mui/material/Grid";
import CustomBreadcrumbs from "./components/CustomBreadcrumbs";
import {useNavigate, useParams} from "react-router-dom";
import SaveIcon from '@mui/icons-material/Save';
import {
    Button,
    Card,
    FormControl,
    FormGroup, InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import constants from "../store/constans";

const ProductEditPage = () => {
    const params = useParams();
    const navigate = useNavigate()
    const productId = params.productId;
    const pathProductDetail = constants.API_URL + 'products';
    const pathProductUpdate = constants.API_URL + 'products';
    const pathCategoriesList = constants.API_URL + 'categories';
    const pathBrandsList = constants.API_URL + 'brands';
    const pathTypesList = constants.API_URL + 'types';
    const [product, setProduct] = useState([]);
    const [categories, setCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [types, setTypes] = useState([]);
    const [isReady, setIsReady] = useState(false);

    const handleChange = (event) => {
        setProduct({
            ...product,
            [event.target.name]: event.target.value,
        })
    };

    const updateHandler = async () => {
        const response = await fetch(pathProductUpdate + '/' + productId, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)

        })
        const res = await response.json()
        navigate('/products')

    }

    const getProductDetailData = async () => {
        const response = await fetch(pathProductDetail + '/' + productId, {
            headers: {
                Accept: 'application/json'
            }
        })
        const product = await response.json()
        setProduct(product)
    }

    const getCategoriesData = async () => {
        const response = await fetch(pathCategoriesList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const categories = await response.json()
        setCategories(categories)
    }

    const getTypesData = async () => {
        const response = await fetch(pathTypesList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const types = await response.json()
        setTypes(types)
    }

    const getBrandData = async () => {
        const response = await fetch(pathBrandsList, {
            headers: {
                Accept: 'application/json'
            }
        })
        const brands = await response.json()
        setBrands(brands)
    }


    useEffect(() => {
        getProductDetailData()
            .then(() => {
                getCategoriesData()
            }).then(() => {
            getTypesData()
        }).then(() => {
            getBrandData()
        }).then(() => {
            setIsReady(true)
        }).catch(() => {
            alert('Algo salio Mal!')
        })
    }, [])

    return (
        <Layout>
            <Grid item xs={12} md={12} lg={12}>
                <CustomBreadcrumbs title={"Product"}></CustomBreadcrumbs>
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
                <Card sx={{backgroundColor: "#c5cae9", padding: 10}}>
                    <Typography sx={{marginBottom: 3}} component="div" variant="h3">
                        Editar Producto
                    </Typography>
                    {isReady && <FormGroup>
                        <FormControl>
                            <TextField
                                sx={{marginTop: 3, marginBottom: 3}}
                                type="number"
                                label="Código"
                                value={product.id}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                sx={{marginTop: 3, marginBottom: 3}}
                                label="Descripcion"
                                name="description"
                                value={product.description}
                                onChange={handleChange}
                            />
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Categoria</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                name="category_id"
                                value={product.category_id}
                                onChange={handleChange}
                            >
                                {categories.map(item => <MenuItem key={item.id}
                                                                  value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                labelId="demo-simple-select-label"
                                name="type_id"
                                id="demo-simple-select"
                                value={product.type_id}
                                onChange={handleChange}
                            >
                                {types.map(item => <MenuItem key={item.id}
                                                             value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Marca</InputLabel>
                            <Select
                                sx={{marginTop: 3, marginBottom: 3}}
                                labelId="demo-simple-select-label"
                                name="brand_id"
                                id="demo-simple-select"
                                value={product.brand_id}
                                onChange={handleChange}
                            >
                                {brands.map(item => <MenuItem key={item.id}
                                                              value={item.id}>{item.description}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="Código proveedor"
                                sx={{marginTop: 3, marginBottom: 3}}
                                value={product.supplier_code}
                                name="supplier_code"
                                onChange={handleChange}
                            />
                        </FormControl>

                        <Divider/>
                        <div>
                            <Grid item xs={4} md={6} lg={6}>
                                <Button sx={{marginTop: 3, justifyContent: "center"}} variant="contained"
                                        endIcon={<SaveIcon/>} onClick={updateHandler}>
                                    Save
                                </Button>
                            </Grid>
                        </div>

                    </FormGroup>}
                </Card>
            </Grid>

        </Layout>);
}

export default ProductEditPage;