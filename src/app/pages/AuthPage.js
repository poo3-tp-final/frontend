import React, {Fragment} from "react";
import MainNavigation from "../layout/components/app-bar/MainNavigation";
import CustomFooter from "../layout/components/Footer/CustomFooter";
import AuthForm from "../layout/components/login/AuthForm";
import { Container} from "@mui/material";

const AuthPage = () => {
    return (
        <Fragment>

            <MainNavigation title="Tp-POO3" urlLogo="https://i.imgur.com/87sa9Wh.png">
            </MainNavigation>
            <Container>

                    <AuthForm></AuthForm>
            </Container>
            <CustomFooter title="&copy; Maxi Rodriguez, Marcelo Esperanza. Trabajo Practico."
                          shortTitle="&copy; MaxiR "></CustomFooter>

        </Fragment>
    );
}
export default AuthPage;