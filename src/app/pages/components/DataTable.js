import * as React from 'react';
import MUIDataTable from "mui-datatables";
import {useEffect, useState} from "react";
import constants from "../../store/constans";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    IconButton,
    Slide
} from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {useNavigate} from "react-router-dom";
import {Fragment} from "react";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function DataTable() {
    const [products, setProducts] = useState([]);
    const [productId, setProductId] = useState(null);
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const path = constants.API_URL + 'products';


    const handleClose = () => {
        setProductId(null)
        setOpen(false);
    };

    const handleRemove = () => {
        removeProduct().then(() => {
            setProductId(null)
            setOpen(false);
        }).then(() => {
            navigate(0)
        })
    }


    const onEditHandler = (value) => {
        let id = value.rowData[0]
        let pathto = `/products/${id}`;
        navigate(pathto)
    }

    const onRemoveHandler = (value) => {
        setProductId(value.rowData[0])
        setOpen(true);
    }

    const getData = async () => {
        const response = await fetch(path, {
            headers: {
                Accept: 'application/json'
            }
        })
        const productos = await response.json()
        setProducts(productos)
    }

    const removeProduct = async () => {
        let pk = productId
        console.log(path + '/' + pk)
        const response = await fetch(path + '/' + productId, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            }
        })

    }

    useEffect(() => {
        getData()
    }, [])

    const columns = [
        {
            name: "id",
            label: "ID"
        },
        {
            name: "description",
            label: "DESCRIPCION"
        },
        {
            name: "type",
            label: "TIPO"
        },
        {
            name: "brand",
            label: "MARCA"
        },
        {
            name: "category",
            label: "CATEGORIA"
        },
        {
            name: "created",
            label: "CREADO"
        },
        {
            name: "supplier_code",
            label: "CODIGO"
        },
        {
            name: "active",
            label: "ACTIVO"
        },
        {
            name: "actions",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <>
                            <IconButton onClick={() => onEditHandler(tableMeta)}>
                                <EditIcon sx={{color: "blue"}}/>
                            </IconButton>
                            <IconButton onClick={() => onRemoveHandler(tableMeta)}>
                                <DeleteForeverIcon sx={{color: "red"}}/>
                            </IconButton>
                        </>
                    )
                }
            }
        }

    ]


    return (
        <Fragment>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle>{"Seguro quiere eliminar este producto?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        El producto seleccionado se eliminara permanentemente de la base de datos.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Atras</Button>
                    <Button sx={{color: "red"}} onClick={handleRemove}>Eliminar</Button>
                </DialogActions>
            </Dialog>
            <MUIDataTable
                title={"Lista de Productos"}
                data={products}
                columns={columns}

            />
        </Fragment>
    );
}