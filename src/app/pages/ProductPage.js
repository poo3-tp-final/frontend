import React from "react";
import Layout from "../layout/Layout";
import DataTable from "./components/DataTable";
import Grid from "@mui/material/Grid";
import CustomBreadcrumbs from "./components/CustomBreadcrumbs";
import {Fab} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import Box from "@mui/material/Box";
import {useNavigate} from "react-router-dom";

const ProductPage = () => {
const navigate = useNavigate()

    return (
        <Layout>
            <Grid item xs={12} md={12} lg={12}>
            <CustomBreadcrumbs title={"Products"}></CustomBreadcrumbs>
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
            <DataTable ></DataTable>

            </Grid>
            <Box sx={{ '& > :not(style)': {
                    position: 'absolute',
                    bottom: 70,
                    right: 50} }} onClick={()=> { navigate('/products/create')}}>
                <Fab color="primary" aria-label="add">
                    <AddIcon />
                </Fab>
            </Box>

        </Layout>);
}

export default ProductPage;